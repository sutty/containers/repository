#!/bin/sh
set -e

export PATH=/usr/lib/ccache/bin:$PATH

cd /home/builder/package

abuild checksum
abuild -r
